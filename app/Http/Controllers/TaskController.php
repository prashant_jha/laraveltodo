<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;



class TaskController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    //display all the task
    public function displayTasks(){
        $user = Auth::user();
        $tasks = $user->tasks;

        // Extract all the tasks of the current user in ascending order 
        $tasks = Task::orderBy('created_at', 'asc')->get();
        return view('home',[
            'tasks' => $tasks
        ]);
    }


    //Function to create a new task
    public function createTask(Request $request){

        //Validate the task name field for maximum of 255 char
        $validator = Validator::make($request->all(),[
            'task' => 'required|max:255'
        ]);


        //Redirect user to the tasks page if there are any errors in the validation with all the errors 
        if ($validator->fails()){
            return Response::json(['success'=>false, 'errors'=>$validator->errors()->all()]);
            // return redirect('/tasks')->withInput()->withErrors($validator);

        }

        //Get the current user so to store the task in the user's one to many field
        $user = Auth::user();
        
        $task = new Task;
        $task->name = $request->task;
        
        // Add the task to the user's task field and in the task table 
        $task_obj = $user->tasks()->save($task);
        
        return Response::json(['success'=>true, 'task_data'=>['name'=>$task->name,'id'=>$task_obj->id]]);

        // return redirect('/tasks');
    }   



    public function deleteTask(Request $request, $id){

        //Find the task with the particular id or return 404 if not present and delete it
        Task::findOrFail($id)->delete();
        
        //After successful deletion of the task redirect user to the tasks page
        return redirect('/tasks'); 
    }


}
