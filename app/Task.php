<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    //Task to Uer relationship i.e. ManyToOne
    public function user(){
        return $this->belongsTo(User::class);
    }
}
