@extends('layouts.app')

@section('content')
<div class="panel-body col-10 offset-1 col-sm-8 offset-sm-2 col-md-6 offset-md-3 p-0" style="border:1px solid lightgray;">

        <div class="bg-white m-0 border-bottom p-2">
            <span class="lead align-middle">New Task</span>
        </div>

        <!-- New Task Form -->
        <form id="task-form" class="form-horizontal" >
            @csrf

            <!-- Task Name -->
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label font-weight-bold mt-2">Task</label>

                <div class="pl-2 pr-2" id="task-field">
                    <input type="text" name="name" id="task-name" class="form-control ">
                    @if($errors->has('name'))
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    @endif
                </div>
            </div>

            <!-- Add Task Button -->
            <div class="form-group ">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default border border-secondary">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>
        </form>
</div>

    <!-- TODO: Current Tasks -->

<div class="panel-body mt-5 col-10 offset-1 col-sm-8 offset-sm-2 col-md-6 offset-md-3 p-0" style="border:1px solid lightgray;">

    @if (count($tasks) > 0)
        <div class="panel panel-default">
            <div class="bg-white m-0 border-bottom p-2">
                <span class="lead align-middle">Current Task</span>
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>Task</th>
                        <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody id="task-data">
                        @foreach ($tasks as $task)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    <div>{{ $task->name }}</div>
                                </td>

                                <td class="text-right">
                                    <form id="delete-task">
                                        @csrf
                                        <button type="submit" class="btn btn-default border border-secondary bg-danger">
                                            <span class="text-white">Delete Task</span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
</div>

@endsection

@section('javascript')

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script type="text/javascript">

    $("#task-form").submit(function(event){
        event.preventDefault();
        var _token = $("input[name='_token']").val();
        var task = $("input[name='name']").val();

        $.ajax({
            url: '/task',
            type: 'POST',
            datatype: 'JSON',
            data : {
                _token: _token,
                task: task,
            },

            success: function(data){
                console.log(data)
                if(data.success){
                    if($("#error-message").length != 0){
                        $("#error-message").remove();
                    }
                    appendTask(data.task_data);

                    swal("Success", "Task Added Successfully", "success");
                }
                else{
                    appendErrors(data.errors);
                }

            }
        });
    });

    function appendTask(task_data) {
        $('#task-data').append("<tr> <td class='table-text'> <div class='text-left'>"+task_data.name+"</div> </td> <td class='text-right'> <form action='/task/' "+task_data.id+"method='POST' <button type='submit' class='btn btn-default border border-secondary bg-danger'> <span class='text-white'>Delete Task</span> </button> </form> </td> </tr>");
    }

    function appendErrors(errors){
        $('#task-field').append("<div id='error-message'><p class='text-danger'>"+ errors.name +"</p></div>")
    }

</script>


@endsection