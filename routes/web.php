<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/tasks', 'TaskController@displayTasks')->name('tasks');

Route::post('/task', "TaskController@createTask")->name('create_task');

Route::post('/task/{id}', 'TaskController@deleteTask')->name('delete_task');

Route::post('/delete_task', 'TaskController@deleteTask')->name('delete_task');
